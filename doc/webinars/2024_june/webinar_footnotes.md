# Cноски/заметки к вебинарам

***Ссылки ниже не аффилированы с LMStudio и МАЦБКТ, и представлены исключительно для ознакомительных целей.***

## Вебинар 1
- Сносок нет

## Вебинар 2
- Blender: скачивание версии 4.1.1
	- https://download.blender.org/release/Blender4.1/
- Blender: документация версии 4.1.1
	- https://docs.blender.org/manual/ru/4.1/
- Blender: обучение азам версии 4.x
	- https://www.youtube.com/watch?v=3Ru613FJDXU&list=PLkxXQ3ugQK2N2PPxmeOivCT4tP4ne_V1y
- Godot: скачивание версии 4.2.2
	- https://godotengine.org/download/archive/
- Godot: введение
	- https://docs.godotengine.org/ru/4.x/getting_started/introduction/introduction_to_godot.html
- Godot: главные концепции 
	- https://docs.godotengine.org/ru/4.x/getting_started/introduction/key_concepts_overview.html
- Godot: файловая система
	- https://docs.godotengine.org/ru/4.x/tutorials/scripting/filesystem.html
- Godot: узлы и сцены
	- https://docs.godotengine.org/ru/4.x/getting_started/step_by_step/nodes_and_scenes.html
- Godot: ресурсы
	- https://docs.godotengine.org/ru/4.x/tutorials/scripting/resources.html
- Godot: материалы 3D
	- https://docs.godotengine.org/ru/4.x/tutorials/3d/standard_material_3d.html
- Godot: статьи по физической симуляции в движке
	- https://docs.godotengine.org/ru/4.x/tutorials/physics/index.html
- Godot: освещение и тени
	- https://docs.godotengine.org/ru/4.x/tutorials/3d/lights_and_shadows.html
- Godot: окружение и постобработка
	- https://docs.godotengine.org/ru/4.x/tutorials/3d/environment_and_post_processing.html

## Вебинар 3
- Godot: статьи по GDScript
	- https://docs.godotengine.org/ru/4.x/tutorials/scripting/gdscript/index.html
- Godot: создание первой 3D игры
	- https://docs.godotengine.org/ru/4.x/getting_started/first_3d_game/index.html
- Godot: курс по Godot для начинающих
	- https://www.youtube.com/watch?v=z23MQ2xad30&list=PLpp4UXjsd_VeN7FrIbk7suElcL3bm6eLB
- Godot: перевод курса по GDScript
	- https://tmoeg.itch.io/learn-godots-gdscript-from-zero-russian-language
- Godot: англоязычная база знаний/уроков по Godot
	- https://kidscancode.org/godot_recipes/4.x/g101/index.html
- Godot: основы GDScript
	- https://docs.godotengine.org/ru/4.x/tutorials/scripting/gdscript/gdscript_basics.html
- Godot: обзор глобальных функци и переменных GDScript
	- https://docs.godotengine.org/en/stable/classes/class_@globalscope.html
- Godot: анимация объектов
	- https://docs.godotengine.org/ru/4.x/tutorials/animation/index.html